import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Main {
	public static void getReponse(BufferedReader brSock) throws IOException {
		String result = "";
		do {
			// on lit ligne par ligne et on affiche le message dès que l'on peut
			result = brSock.readLine();
			System.out.println(result);
			/* tant que les messages reçus commencent par un "1", 
			le serveur n'a pas fini d'envoyer les réponses donc on continue de lire */
		} while(result.startsWith("1")); 
	}
	
	public static void main(String [] args) throws Exception {
		Socket client = new Socket("localhost", 11181); // connexion au réseau local sur le port fournit en second paramètre
		String commande = "";
		PrintStream ps = new PrintStream( client.getOutputStream() ); // création du canal de contrôle

		// création du flot des réponses du serveur
		BufferedReader brSock = new BufferedReader(new InputStreamReader(client.getInputStream()));

		// création du flot de lecture des commandes saisies par l'utilisateur
		BufferedReader brCons = new BufferedReader(new InputStreamReader(System.in)); 
		
		System.out.println(System.getProperty("user.dir"));
		// je lis les messages de bienvenue
		System.out.println(brSock.readLine());
		System.out.println(brSock.readLine());
		System.out.println(brSock.readLine());
		
		// j'�cris des commandes sur la console
		while(!(commande = brCons.readLine()).equals("bye")) {
			/* - dans le cas de la commande get, on indique en second argument 
				l'endroit (côté client) où le fichier devra se téléchargé 
	
			   - dans le cas de la command store, on indique en second argument
			   l'emplacement (côté client) du fichier à upload
			*/
			if(commande.split(" ")[0].equals("get") || commande.split(" ")[0].equals("stor"))
				commande = commande + " " + System.getProperty("user.dir");		
			try { // on tente d'envoyer la commande saisie au serveur, et de recevoir la réponse correspondante
				ps.println(commande);
				getReponse(brSock); 
			}
			// si une exception est levée, cela signifie que la connexion avec le serveur n'est pas fonctionnelle 
			catch(Exception e) { 
				System.out.println("Le serveur s'est déconnecté");

				// fermeture des canaux de communications
				ps.close();
				brSock.close();
				brCons.close();

				// fermeture du socket client
				client.close(); 
			}
		}
		ps.println(commande);
		
		// fermeture des canaux de communications
		ps.close();
		brSock.close();
		brCons.close();

		// fermeture du socket client
		client.close();  
	}
}
