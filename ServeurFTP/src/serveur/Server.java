package serveur;
import java.net.ServerSocket;

public class Server {
	public static void main(String[] args) throws Exception {
		System.out.println("Le serveur FTP");
		System.out.println(System.getProperty("user.dir"));
	    try
	    {
	      ServerSocket ss = new ServerSocket(11181); // ouverture d'un socket serveur sur port
	      while (true) // attente en boucle de connexion (bloquant sur ss.accept)
	        new ServeurThread(ss.accept()); // un client se connecte, un nouveau thread client est lancé
	    }
	    catch (Exception e) { }
	}
}
