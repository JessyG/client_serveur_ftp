package serveur;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import serveur_commandes.CommandExecutor;

public class ServeurThread implements Runnable {
	
	private Thread t_cli; // contiendra le thread du client
	private Socket sock; // recevra le socket liant au client
	BufferedReader br;
	PrintStream ps;

	public ServeurThread(Socket s) {
		this.sock = s;
		try {
			this.br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			this.ps = new PrintStream(s.getOutputStream());
		}
		catch (IOException e){ }

		this.t_cli = new Thread(this); // instanciation du thread
    	this.t_cli.start(); // démarrage du thread
	}
	
	public void run() {
		System.out.println("Nouveau client");
		
		this.ps.println("1 Bienvenue ! ");
		this.ps.println("1 Serveur FTP Personnel.");
		this.ps.println("0 Authentification : ");
		
		String commande = "";
		
		CommandExecutor cmdExec = new CommandExecutor(this.ps);
		
		// Attente de reception de commandes et leur execution
		try {
			while(!(commande=this.br.readLine()).equals("bye")) {
				System.out.println(">> "+ commande);
				cmdExec.executeCommande(commande);
			}
			System.out.println("Le client s'est déconnecté");
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			this.ps.close();
			try {
				this.br.close();
				this.sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}