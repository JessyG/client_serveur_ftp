package serveur_commandes;
import java.io.File;

public class CommandeUSER extends Commande {

	public CommandeUSER(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande et de ses arguments
	}

	public String execute() {
		if(nbArgumentsCommande() == 0) // aucun arguments
			return "vide";
		File fileTest = new File("rep_utilisateurs/" + commandeArgs[0]);
		if(fileTest.exists())
			return "ok";
		else
			return "existe pas";
	}

}
