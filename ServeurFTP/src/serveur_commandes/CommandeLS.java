// LS : afficher la liste des dossiers et des fichiers du répertoire courant du serveur 

package serveur_commandes;
import java.io.File;

public class CommandeLS extends Commande {
	
	public CommandeLS(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande 
	}

	public String execute(String pathName) {
		String result = "";
		String etat = "1 ";
		File file = new File("rep_utilisateurs/" + pathName);

		/* création d'un fichier vide à l'emplacement où l'on veut exécuter la commande "ls" pour pouvoir ensuite avoir
		  accès à la liste des fichiers qu'il contient avec la méthode list() */
		File f = new File(file.getAbsoluteFile().toString()); 
		String[] ls = f.list();
		if(ls.length == 0)
			return "0 R�pertoire vide";
		File f2;
		// pour chaque fichier du répertoire, on récupère ses propriétés
		for(int i = 0; i < ls.length; i++) {
			f2 = new File("rep_utilisateurs/" + pathName + "/" + ls[i]);
			if(i == ls.length - 1) etat = "0 ";
			result += f2.isDirectory() ? etat + 'd' : etat + '-';
			result += f2.canRead() ? 'r' : '-';
			result += f2.canWrite() ? 'w' : '-';
			result += "\t" + f2.length();
			result += etat.equals("1 ") ? "\t" + f2.getName() + "\n" : "\t" + f2.getName();
		}
		return result;
	}

	@Override
	public String execute() {
		// TODO Auto-generated method stub
		return null;
	}

}
