// CD : pour changer de répertoire courant du côté du serveur 

package serveur_commandes;
import java.io.File;

public class CommandeCD extends Commande {
	
	public CommandeCD(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande et de son argument
	}

	public String execute(String pathName) {
		String newPath = commandeArgs[0];
		File fileTest = new File("rep_utilisateurs/" + pathName + "/" + newPath);
		if(newPath.equals("..")) {
			if(pathName.contains("/"))
				return "0 " + pathName.substring(0, pathName.lastIndexOf("/"));
			else
				return "0 " + pathName;
		}
		if(fileTest.exists()) {
			if(fileTest.isDirectory())
				return "0 " + pathName + "/" + newPath;
			else
				return "2 " + newPath + " n'est pas un dossier";
		}
		return "2 " + newPath + " n'existe pas";
	}

	@Override
	public String execute() {
		// TODO Auto-generated method stub
		return null;
	}

}
