// GET : pour télécharger un fichier du serveur vers le client 

package serveur_commandes;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CommandeGET extends Commande {
	
	public CommandeGET(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande et de ses arguments
	}

	public String execute(String pathName, String userPath) {
		String fileD = commandeArgs[0]; // fichier à dl
		File fileServer = new File("rep_utilisateurs/" + pathName + "/" + fileD); // chemin (côté serveur) du fichier à download
		if(fileServer.exists()) {
			if(fileServer.isFile()) {
				// Recevoir le fichier 
				System.out.println("PATH : " + userPath + "\\" + fileD);
				// emplacement (côté client) où le fichier sera téléchargé :    userPath + "\\" + fileD  
		        File fileUser = new File(userPath + "\\" + fileD);
		        
		        try {
		        	// Créer un nouveau fichier
					fileUser.createNewFile();

					/* rempli ligne par ligne le contenu du fichier nouvellement crée (côté client) 
					à partir du fichier source (côté serveur) */
					BufferedReader br = new BufferedReader(new FileReader(fileServer));
			        BufferedWriter bw = new BufferedWriter(new FileWriter(fileUser));
			        for(String l = br.readLine(); l != null; l = br.readLine()) {
			            bw.write(l);
			            bw.newLine();
			        }
			        // fermeture des canaux de communication qui ont permis le transfert du fichier
			        br.close();
			        bw.close();
			        return "0 fichier téléchargé";
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else
				return "2 " + fileD + " doit être un fichier";
		}
		return "2 ce fichier n'existe pas";
	}
	
	@Override
	public String execute() {
		return null;
	}

}
