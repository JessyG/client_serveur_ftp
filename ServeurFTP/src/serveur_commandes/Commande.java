package serveur_commandes;


public abstract class Commande {
	protected String commandeNom = "";
	protected String [] commandeArgs ;
	protected String [] args;
	
	public Commande(String commandeStr) {
		args = commandeStr.split(" ");
		commandeNom = args[0];
		commandeArgs = new String[args.length-1];
		
		for(int i=0; i<commandeArgs.length; i++) {
			commandeArgs[i] = args[i+1];
		}
	}
	
	public int nbArgumentsCommande() {
		return this.args.length - 1;
	}
	
	public abstract String execute();

}
