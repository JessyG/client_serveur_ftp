// STOR : pour envoyer un fichier vers le dossier courant serveur

package serveur_commandes;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CommandeSTOR extends Commande {
	
	public CommandeSTOR(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande et de ses arguments
	}

	public String execute(String pathName, String userPath)  {
		String fileD = commandeArgs[0]; // fichier à dl
		File fileUser = new File(userPath + "\\" + fileD); // chemin (côté client) du fichier à upload
		if(fileUser.exists()) {
			if(fileUser.isFile()) {
				// Recevoir le fichier
				// emplacement (côté serveur) où le fichier sera reçu :    rep_utilisateurs/" + pathName + "/"  
				File fileServer = new File("rep_utilisateurs/" + pathName + "/" + fileD);
				try {
					// Créer un nouveau fichier
					fileServer.createNewFile();

					/* remplit ligne par ligne le contenu du fichier nouvellement crée (côté serveur) 
					à partir du fichier source (côté client) */
					BufferedReader br = new BufferedReader(new FileReader(fileUser));
					BufferedWriter bw = new BufferedWriter(new FileWriter(fileServer));
					for(String l = br.readLine(); l != null; l = br.readLine()) {
						bw.write(l);
			            bw.newLine();
			         }
			         // fermeture des canaux de communication qui ont permis le transfert du fichier
			         br.close();
			         bw.close();
			         return "0 fichier envoyé au serveur";
				} catch (IOException e) {
					e.printStackTrace();
				}   
			}
			return "2 " + fileD + " doit être un fichier";
		}
		return "2 ce fichier n'existe pas";
	}

	@Override
	public String execute() {
		// TODO Auto-generated method stub
		return null;
	}

}
