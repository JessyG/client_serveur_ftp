// PASS : pour envoyer le mot de passe 
package serveur_commandes;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommandePASS extends Commande {
	
	
	public CommandePASS(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande et de ses arguments
	}

	public String execute(String pathName) throws IOException {
		// création d'un flot de lecture sur le fichier contenant les mots de passe des utilisateurs
		BufferedReader br = new BufferedReader(new FileReader("rep_utilisateurs/" + pathName + "/pw.txt"));
		if(nbArgumentsCommande() == 0) { // aucun arguments 
			br.close();
			return "vide";
		}
		// le mdp saisi correspondant à celui lu dans la base de données
		else if(commandeArgs[0].toLowerCase().equals(br.readLine())) {
			br.close();
			return "ok";
		}
		else { // mdp erroné
			br.close();
			return "existe pas";
		}
		
	}

	@Override
	public String execute() {
		// TODO Auto-generated method stub
		return null;
	}
}
