// classe contenant les informations relatives au compte d'un utilisateur et à sa connexion

package serveur_commandes;
public class User {
	protected String userPath;
	protected String pw;
	protected boolean userOk;
	protected boolean pwOk;
	
	public User()
	{
		this.userOk = false;
		this.pwOk = false;
	}
	
	public String getUserPath() {
		return this.userPath;		
	}
	
	public void setUserPath(String newPath) {
		this.userPath = newPath;
	}
	
	public String getPw() {
		return this.pw;		
	}
	
	public void setPw(String pw) {
		this.pw = pw;
	}
	
	public boolean getUserOk() {
		return this.userOk;		
	}
	
	public void setUserOk() {
		this.userOk = true;
	}
	
	public void setUserNotOk() {
		this.userOk = false;
	}
	
	public boolean getPwOk() {
		return this.pwOk;		
	}
	
	public void setPwOk() {
		this.pwOk = true;
	}
	
	public void setPwNotOk() {
		this.userOk = false;
	}
	

}
