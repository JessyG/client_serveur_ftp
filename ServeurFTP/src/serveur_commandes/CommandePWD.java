// PWD : pour afficher le chemin absolu du dossier couran

package serveur_commandes;
import java.io.File;

public class CommandePWD extends Commande {
	
	public CommandePWD(String commandeStr) {
		super(commandeStr); // extraction du nom de la commande 
	}

	public String execute(String path) {
		// création d'un fichier vide dans le répertoire courant
		File file = new File(path);

		// on renvoie le chemin absolu de ce fichier, pour connaître l'emplacement du répertoire courant
		return "0 " + file.getAbsoluteFile().toString();
	}

	@Override
	public String execute() {
		// TODO Auto-generated method stub
		return null;
	}

}
