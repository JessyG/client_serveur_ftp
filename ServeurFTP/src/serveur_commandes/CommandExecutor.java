package serveur_commandes;
import java.io.IOException;
import java.io.PrintStream;

import serveur_commandes.User;

public class CommandExecutor {
	protected User user;
	protected PrintStream ps;
	protected String result;
	protected String clientPath;
	
	public CommandExecutor(PrintStream ps) {
		this.user = new User();
		this.result = "";
		this.ps = ps;
	}
	
	public void executeCommande(String commande) throws IOException {
		System.out.println(this.user.getUserOk());
		System.out.println(this.user.getPwOk());
		if(this.user.getUserOk() && this.user.getPwOk()) {
			// Changer de repertoire. Un (..) permet de revenir au repertoire superieur
			if(commande.split(" ")[0].equals("cd")) {
				result = (new CommandeCD(commande)).execute(this.user.getUserPath());
				this.ps.println(result);
				this.ps.flush();
				if(result.startsWith("0"))
					user.setUserPath(result.substring(2));
			}
	
			// Telecharger un fichier
			if(commande.split(" ")[0].equals("get")) {
	             clientPath = commande.substring(commande.indexOf(this.user.getUserPath()) + this.user.getUserPath().length() + 1, commande.length());
	             this.ps.println((new CommandeGET(commande)).execute(this.user.getUserPath(),clientPath));
			}
			
			// Afficher la liste des fichiers et des dossiers du repertoire courant
			if(commande.split(" ")[0].equals("ls")) {
				result = (new CommandeLS(commande)).execute(this.user.getUserPath());
				this.ps.println(result);
				this.ps.flush();
			} 
		
			// Afficher le repertoire courant
			if(commande.split(" ")[0].equals("pwd")) {
				result = (new CommandePWD(commande)).execute(this.user.getUserPath());
				this.ps.println(result);
			}
			// Envoyer (uploader) un fichier
			if(commande.split(" ")[0].equals("stor")) {
				clientPath = commande.split(" ")[2];
	            this.ps.println((new CommandeSTOR(commande)).execute(this.user.getUserPath(),clientPath));
			}
		}
		else {
			if(commande.split(" ")[0].equals("pass") || commande.split(" ")[0].equals("user")) {
				// Le mot de passe pour l'authentification
				if(commande.split(" ")[0].equals("pass")) { 
					result = (new CommandePASS(commande)).execute(this.user.getUserPath());
					
					if(result == "ok") {
						this.ps.println("1 Commande pass OK");
						this.ps.println("0 Vous êtes bien connecté sur notre serveur");
						this.user.setPw(commande.split(" ")[1]); // on sauvegarde le mot de passe saisie car il est juste
						this.user.setPwOk(); // on passe à "true" la variable pwOk
					}
					else if(result == "existe pas"){
						this.ps.println("2 Le mode de passe est faux");
					}
					else if(result == "vide") {
						this.ps.println("2 usage : pass <motDePasse>");
					}
				}
				// Le login pour l'authentification
				if(commande.split(" ")[0].equals("user")) {
					result = (new CommandeUSER(commande)).execute();
					if(result.equals("ok")) {
						this.ps.println("0 Commande user OK");
						this.user.setUserOk();  // on passe à "true" la variable userOk
						this.user.setUserPath(commande.split(" ")[1]); // on sauvegarde le nom d'utilisateur saisie car il est juste
					}
					else if(result.equals("vide")) {
						this.ps.println("2 usage : user <nomUtilisateur>");
					}
					else if(result.equals("existe pas")){
						this.ps.println("2 Le user " + commande.split(" ")[1] + " n'existe pas");
					}
				}
			}
			else
				this.ps.println("2 Vous n'êtes pas connecté !");
		}
	}

}
