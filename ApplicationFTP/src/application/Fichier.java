package application;

import javafx.beans.property.SimpleStringProperty;

public class Fichier {
	private final SimpleStringProperty rights;
	private final SimpleStringProperty size;
	private final SimpleStringProperty name;
     
     public Fichier(String rights, String size, String name) {
         this.rights = new SimpleStringProperty(rights);
         this.size = new SimpleStringProperty(size);
         this.name = new SimpleStringProperty(name);
     }
     
     public String getRights() {
         return this.rights.get();
     }
     
     public String getSize() {
         return this.size.get();
     }
     
     public String getName() {
         return this.name.get();
     }
     
     public SimpleStringProperty getSizeProperty(){
    	 return this.rights;
     }

     public SimpleStringProperty lastNameProperty(){
    	return this.size;
     }

    public SimpleStringProperty getNameProperty(){
    	 return this.name;
    }
}
