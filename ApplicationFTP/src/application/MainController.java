package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class MainController implements Initializable {
	@FXML
	// TextFiel pour l'identifiant
	private TextField t_hote;
	@FXML
	private PasswordField p_password;
	@FXML
	private ListView <String> list_server;
	@FXML
	private TreeView <String> tree_client;
	@FXML
	ButtonBase but_connexion;
	@FXML
	Label l_connexion;
	@FXML
	Label l_pwd;
	
	private Socket client;
	private BufferedReader brSock;
	private PrintStream ps;
	private String result_com = "";
	private String result_ligne = "";
	private final String basePath = System.getProperty("user.dir");
	private boolean connexion_ok = false;
	
	/*
	 * � l'initialisation :
	 * -> on d�tecte l'�v�nement "double-clique" sur la ListView cot� serveur
	 * et sur le TreeView cot� client
	 * -> on remplit le TreeView
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		test_list();
		test_tree();
		File rep_courant = new File(basePath);
		tree_client.setRoot(getNodesForDirectory(0, rep_courant));
	}
	
	/*
	 * Fonction pour actualiser le TreeView
	 * utile lors de l'appel de la commande GET afin de pouvoir
	 * visualiser le fichier t�l�charger depuis le server vers le client
	 */
	private void act_tree() {
	    tree_client.getRoot().getChildren().clear();
	    tree_client.setRoot(null);
	    File rep_courant = new File(basePath);
		tree_client.setRoot(getNodesForDirectory(0, rep_courant));
	}
	
	/*
	 * Fonction pour lire les r�ponses envoy�es par le serveur
	 */
	public String getReponse() throws IOException {
		do {
			result_ligne = brSock.readLine();
			System.out.println(result_ligne);
		} while(result_ligne.startsWith("1"));
		return result_ligne.substring(0, 1);
	}
	
	/*
	 * Fonction pour utiliser la commande LS :
	 * -> on efface la ListView du serveur
	 * -> on envoie "ls" au serveur
	 * -> on lit les r�ponses (= la liste des fichiers du r�pertoire courant))
	 * -> on remplit � nouveau la ListView
	 */
	public void ls_use() throws IOException {
		list_server.getItems().clear();
		ps.println("ls");
		list_server.getItems().add("..");
		do {
			result_ligne = this.brSock.readLine();
			if(!result_ligne.equals("0 R�pertoire vide"))
				list_server.getItems().add(result_ligne.substring(2, result_ligne.length()));
			System.out.println(result_ligne);
		} while(result_ligne.startsWith("1"));
	}
	
	/*
	 * Fonction pour utiliser PWD
	 * -> permet de mettre � jour le label l_pwd apr�s un appel de la commande CD
	 */
	public void pwd_use() throws IOException {
		ps.println("pwd");
		test_serveur();
		l_pwd.setText(result_ligne.substring(2, result_ligne.length()));
	}
	
	/*
	 * Fonction pour se connecter au serveur
	 * lorsque l'on clique sur le bouton "connexion"
	 */
	@FXML
	public void connexion() throws IOException {
		// on r�cup�re les donn�es ins�r�es dans les TextField
		String hote = t_hote.getText();
		String password = p_password.getText();
		
		// connexion au serveur
		this.client = new Socket("localhost", 11181);
		this.ps = new PrintStream( client.getOutputStream() );
		this.brSock = new BufferedReader(new InputStreamReader(client.getInputStream()));
			
		System.out.println(brSock.readLine());
		System.out.println(brSock.readLine());
		System.out.println(brSock.readLine());
		
		// test de la commande USER
		ps.println("user " + hote);
		test_serveur();
		// si l'identifiant est correct
		if(!result_com.equals("2")) {
			// alors on teste le mot de passe
			ps.println("pass " + password);
			test_serveur();
			// si le mot de passe est correct
			if(!result_com.equals("2")) {
				// alors on appelle les commandes LS et PWD
				// afin de remplir le Label l_pwd et la ListView
				// du client
				l_connexion.setText("");
				connexion_ok = true;
				ls_use();
				pwd_use();
				// le bouton de connexion devient alors un bouton de d�connexion
				// afin de permettre au client et au thread s'occupant du client
				// de s'arr�ter correctement
				this.but_connexion.setText("D�connexion");
				this.but_connexion.setOnAction((event) -> {
					try {
						deconnexion();
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			}
			else {
				l_connexion.setText("Mot de passe incorrect");
			}
		}
		else {
			l_connexion.setText("Identifiant incorrect");
		}
	}
	
	/*
	 * Fonction pour tester le bon fonctionnement du serveur
	 * en v�rifiant que le client peut bien lire les r�ponses
	 * qui lui sont envoy�es
	 * si ce n'est pas le cas
	 * alors le client est pr�venu et est d�connect�
	 */
	public void test_serveur() throws IOException {
		try {
			result_com = getReponse();
		} catch (Exception e) {
			System.out.println("Serveur d�connect�");
			ps.close();
			brSock.close();
			client.close();
			e.printStackTrace();
		}
	}
	
	/*
	 * Fonction pour d�tecter l'�v�nement "double-clique" sur un item
	 * de la ListView du serveur
	 * si l'�l�ment s�lectionn� est un r�pertoire alors :
	 * 	-> on appelle la commande CD pour modifier le r�pertoire courant
	 * 	-> on appelle ensuite la commande LS pour mettre � jour la ListView
	 * sinon, l'�l�ment s�lectionn� est un fichier, et donc :
	 * -> on appelle la commande GET pour t�l�charger le fichier s�lectionn�
	 * vers le r�pertoire courant du client
	 */
	public void test_list() {
		list_server.setOnMousePressed(new EventHandler<MouseEvent>() {
			    @Override
			    public void handle(MouseEvent click) {
			        if (click.getClickCount() == 2) {
			        	// dans res[0] se trouve les droits du fichier
			        	// dans res[1] sa taille
			        	// dans res[2] son nom
			        	String[] res = list_server.getSelectionModel().getSelectedItem().split("\t");
			        	// si la selection est un dossier alors on appelle la commande CD, puis LS
			        	if(res[0].startsWith("d") || res[0].equals("..")) {
			        			if(res[0].equals(".."))
			        				ps.println("cd ..");
			        			else
			        				ps.println("cd " + res[2]);
			        			try {
									test_serveur();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
			        			// Si la commande CD s'est bien pass�e, on appelle la commande ls
			        			if(!result_com.equals("2")) {
			        				try {
			        					pwd_use();
										ls_use();
									} catch (Exception e) {
										System.out.println("Serveur d�connect�");
										e.printStackTrace();
									}
			        			}
			        	}
			        	// si c'est un fichier alors on appelle la commande get
			        	else {
			        		ps.println("get " + res[2] + " " + System.getProperty("user.dir"));
			        		try {
								test_serveur();
								act_tree();
							} catch (IOException e) {
								e.printStackTrace();
							}
			        	}
			        }
			    }
		});
	}
	
	/*
	 * Fonction pour r�cup�rer le chemin absolu de
	 * l'�l�ment s�lectionn� dans le TreeView du client
	 * 
	 * Utile pour la fonction STOR
	 */
	public String get_path_tree() {
		StringBuilder pathBuilder = new StringBuilder();
		TreeItem<String> parent = tree_client.getSelectionModel().getSelectedItem().getParent();
		for (TreeItem<String> item = parent; item != null ; item = item.getParent()) {
		    pathBuilder.insert(0, item.getValue());
		    pathBuilder.insert(0, "\\");
		}
		pathBuilder.insert(0, this.basePath.substring(0, this.basePath.lastIndexOf("\\")));
		return pathBuilder.toString();
	}
	
	/*
	 * Fonction pour d�tecter l'�v�nement double_clique sur un �l�ment de la TreeView du client
	 * Si on double-clique sur un fichier alors :
	 * -> on appelle la commande stor pour t�l�charger le fichier s�lectionn� vers le serveur
	 * -> on appelle la commande ls pour mettre � jour le r�pertoire du serveur
	 * Si on double-clique sur un dossier alors rien ne se passe
	 */
	public void test_tree() {
		tree_client.setOnMousePressed(new EventHandler<MouseEvent>() {
			    @Override
			    public void handle(MouseEvent click) {
			        if (click.getClickCount() == 2) {
			        	if(connexion_ok) {
				        	ps.println("stor " + tree_client.getSelectionModel().getSelectedItem().getValue() + " " + get_path_tree());
				        	try {
								test_serveur();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
				        	// si la commande stor est un succ�s alors on appelle la commande ls
				        	// pour mettre � jour le serveur
				        	if(!result_com.equals("2")) {
				        		try {
									ls_use();
								} catch (IOException e) {
									System.out.println("Le serveur s'est d�connect�");
									e.printStackTrace();
								}
				        	}
			        	}
			        }
			    }
		});
	}
	
	/*
	 * Fonction pour r�cup�rer les fichiers et sous-dossiers
	 * � partir d'un r�pertoire racine
	 * 
	 * N�cessaire pour remplir le TreeView du client
	 */
	public TreeItem<String> getNodesForDirectory(int i, File directory) { //Returns a TreeItem representation of the specified directory
        TreeItem<String> root = new TreeItem<String>(directory.getName());
        for(File f : directory.listFiles()) {
            System.out.println("Loading " + f.getName());
            if(f.isDirectory()) { //Then we call the function recursively
            	if(i < 100)
            		root.getChildren().add(getNodesForDirectory(i + 1, f));
            } else {
                root.getChildren().add(new TreeItem<String>(f.getName()));
            }
        }
        return root;
    }
	
	/*
	 * Fonction pour se d�connecter
	 * On ferme donc tous les flux
	 */
	@FXML
	public void deconnexion() throws IOException {
		connexion_ok = false;
		list_server.getItems().clear();
		l_pwd.setText("");
		ps.println("bye");
		this.ps.close();
		this.brSock.close();
		this.client.close();
		this.but_connexion.setText("Connexion");
		this.but_connexion.setOnAction((event) -> {try {
			connexion();
		} catch (IOException e) {
			e.printStackTrace();
		}});
	}
	
}
